# ticket booking system


## Getting started

The booking system helps to book seats from the train coach.

## Usages

1. Form input will take number of seats as an input. Example: 2 or 4 or 6 or 1 etc.
2. Output will be number of seats that have been booked for the user along with the display of
all the seats.
3. The UI will show the seats availability status through color.

## Use cases

1. There are 80 seats in a coach of a train with only 7 seats in a row and last row of only 3
seats. For simplicity, there is only one coach in this train.
2. One person can reserve up to 7 seats at a time.
3. If a person is reserving seats, the priority will be to book them in one row.
4. If seats are not available in one row then the booking should be done in such a way that the
nearby seats are booked.
5. User can book as many tickets as s/he wants until the coach is full.
6. You don’t have to create login functionality for this application.

## Installation

1. Clone : git clone https://gitlab.com/vinodraju252/ticket-booking-system.git
2. Import db.sql file : mysql -h 127.0.0.1 -u username -p db-name < file-path
3. Change baseUrl in script/script.js file
