<?php 
require_once('../config/db_config.php'); // need to include db connection
require_once('../config/parameters.php'); // need to include required parameters
require_once('../helpers/helpers.php'); // need to include helper for common functions

// Default mesages and default message init for response if request is invaild
$messages = [
	"invaildRequest" => "The input request is not valid, please try again later..!",
	"seatNotAvailable" => "The coach have XX seats available currently, Please try again later once the seats are available..!",
	"bookingResponse" => "The booking request for XX seats has been done successfully...!"
];
$response = [
	"message" => $messages["invaildRequest"]
];

// To check if the request id POST and have `numberOfSeats` key available
if(isset($_POST) && !empty($_POST["numberOfSeats"])) {
	$requiredSeats = (int) $_POST["numberOfSeats"]; // store `numberOfSeats` value
	// Get the stored booking from db from the target function
	$calculateBookings = getStoredData($db, $maxNumberOfSeats, $maxSeatsPerRow);
	// To simplify will create the count array which have each row wise available seats 
	$seatCountByRow = array_map('count', $calculateBookings["available"]);
	// To get total available seats
	$totalAvailableSeats = array_sum($seatCountByRow);
	// To prepare array which have all rows which have available seats grater or equal to requested seats
	$searchIndex = array_filter($seatCountByRow,
	    function ($seatCount) use($requiredSeats) {
	        return ($seatCount >= $requiredSeats);
	    }
	);
	// Condition required to check if available seats more then requested seats
	$checkAvailablity = $totalAvailableSeats < $requiredSeats;

	// if condition fails then message to client for requested seats not available
	if($checkAvailablity) {
		$response = [
			"message" => str_replace("XX", $totalAvailableSeats, $messages["seatNotAvailable"])
		];
	}

	/** 
	 * If the above array_filter function whcih is resposible for providing index of row
	 * Which have available seats have the data
	 * If searchIndex array have the data the code will directly take seats from that
	 */
	if(!empty($searchIndex)) {
		$key = array_key_first($searchIndex);
		$seatNumbers = array_slice($calculateBookings["available"][$key], 0, $requiredSeats);
		$response = bookMySeat($seatNumbers, $db);
	}

	/**
	 * If searchIndex array is empty that means not a single row have all seats available
	 * We have to got to row wise and book the seats
	 */
	if(empty($searchIndex) && !$checkAvailablity) {
		$customSeats = []; // To store the seat numbers
		$needToFill = $requiredSeats; // will use this additonal variable for seat manipulation
		$filteredSeats = getAdjecentSeats($seatCountByRow, $requiredSeats);
		if(!empty($filteredSeats)) {
			$seatCountByRow = $filteredSeats;
		}
		foreach ($seatCountByRow as $index => $seatCount) {
			// Will check if available seats less then requested seats
			if($seatCount <= $needToFill) {
				$customSeats = array_merge($customSeats, $calculateBookings["available"][$index]);
				$needToFill -= $seatCount; // we also decrease the need to fill variable to exist loop
			} elseif($seatCount > $needToFill) { // if available seats more then requested seats
				$seatNumbers = array_slice($calculateBookings["available"][$index], 0, $needToFill);
				$customSeats = array_merge($customSeats, $seatNumbers);
				// we also decrease the need to fill variable to exist loop
				$needToFill -= count($seatNumbers);
			}
			// If all requested seat number prepared the exist from the loop
			if($needToFill == 0){
				break;
			}
		}
		// Call the function to save the seats details to db
		$response = bookMySeat($customSeats, $db);
	}
	/**
	 * if respoonse variable whcih is coming from the function response
	 * This means the seats are books and we also have the booked Ids to indentify how many booked
	 */
	if(!empty($response["booked"])) {
		$response = [
			"message" => str_replace("XX", count($response["booked"]), $messages["bookingResponse"])
		]; 
	}	
}

/**
 * Prepare the respose variable
 */
echo json_encode($response);
exit;