<?php 
require_once('../config/db_config.php'); // need to include db connection
require_once('../config/parameters.php'); // need to include required parameters

// SQL query to fetch the stored booking from db
$sql = "SELECT t.ticket_number, GROUP_CONCAT(td.seat_number) AS seats
FROM tickets AS t
INNER JOIN ticket_details AS td ON td.ticket_id=t.id
WHERE t.status=1
GROUP BY t.ticket_number, t.id ORDER BY t.id DESC"; 
$results = $db->query($sql); // Db query to run
$bookedSeats = [];

// Declaring a array to prepare the respose
$bookings = [
	"recent_bookings" => [],
	"seat_availablity" => []
];

// Get all data from the executed query
while ($row = $results->fetch_assoc()) {
	// Will prepare array for diaplay the last 2 booking, it can be modified later
	if(count($bookings["recent_bookings"]) < $numberOfBookingToDisplay) {
		$bookings["recent_bookings"][] = [
			"ticket_number" => $row["ticket_number"], // will have the ticket number which diaplys to client
			"seats" => $row["seats"], // will have the seat number which diaplys to client
			"seat_count" => count(explode(',', $row["seats"])) // will have the seat count
		];
	}
	// will be having bookedSeats varaible against which we will check if seat is booked or available
	$bookedSeats = array_merge($bookedSeats, explode(',', $row["seats"]));
}

// First row number as we know each row have predefind number of seats
$row = 1;

/**
 * maxNumberOfSeats will have number of seats in the coach
 * maxSeatsPerRow will have seats per row
 * prepare the array which have the available seats in each row
 */
for ($seat=1; $seat<=$maxNumberOfSeats; $seat++) {
	$status = "A"; // here A is status for available seat
	if(in_array($seat, $bookedSeats)){
		$status = "B"; // here B is status for booked seat
	}
	$bookings["seat_availablity"][$row][$seat] = $status;
	if(is_int($seat/$maxSeatsPerRow)){
		$row++;
	}
}

/**
 * Prepare the respose variable
 */
echo json_encode($bookings);
exit;