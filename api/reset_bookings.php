<?php
require_once('../config/db_config.php'); // need to include db connection

try {
	$db->begin_transaction();
	$trancateTicketsSql = $db->prepare("DELETE FROM ticket_details");
	if ($trancateTicketsSql->execute()) {
		$trancateTicketDetailsSql = $db->prepare("DELETE FROM tickets");
		$trancateTicketDetailsSql->execute();
	}
	$response = [
		"message" => "Data has been reseted from the booking db, you can perform the booking again..!"
	];
	$db->commit();
	echo json_encode($response);
	exit;
} catch (Exception $e) {
	$db->rollback();
	$response = [
		"message" => "There is an exception occured while reseting the booking, please try again after some time",
		"exception" => $e->getMessage()
	];

	echo json_encode($response);
	exit;
}