<?php
/**
 * Function will get previouly booked seat details from DB
 *
 * @param Object $db
 * @param int $maxNumberOfSeats
 * @param int $maxSeatsPerRow
 * @return array
 */
function getStoredData(Object $db, int $maxNumberOfSeats, int $maxSeatsPerRow): array
{
	try {
		// Declaring a arry to prepare te return respose from this function
		$calculateBookings = [
			"available" => [], // will store the available seat array
			"booked" => getStoredDataFromQuery($db) // will store the booked seat array
		];
		// First row number as we know each row have predefind number of seats
		$rowNumber = 1;

		/**
		 * maxNumberOfSeats will have number of seats in the coach
		 * maxSeatsPerRow will have seats per row
		 * prepare the array which have the available seats in each row
		 */
		for ($seat=1; $seat<=$maxNumberOfSeats; $seat++) {
			if(!in_array($seat, $calculateBookings["booked"])){
				$calculateBookings["available"][$rowNumber][] = $seat;
			}
			if(is_int($seat/$maxSeatsPerRow)){
				$rowNumber++;
			}
		}
		return $calculateBookings;
	} catch (\Exception $e) { // we can catch the exception in case any error occurs
		// We can use the custom error messageto send to the users instead : $e->getMessage();
		return ["message" => "Failed to create tickets due to exception"];
	}
	
}

function getStoredDataFromQuery(Object $db): array
{
	// SQL query to fetch the stored booking from db
	$sql = "SELECT GROUP_CONCAT(td.seat_number) AS seats
		FROM tickets AS t
		INNER JOIN ticket_details AS td ON td.ticket_id=t.id
		WHERE t.status=1"; 
	$results = $db->query($sql); // Db query to run
	// Get all data from the executed query
	$calculateBookings = [];
	$bookedSeats = $results->fetch_assoc();
	if(!empty($bookedSeats["seats"])) {
		$calculateBookings = explode(',', $bookedSeats["seats"]);
	}

	return $calculateBookings;
}

/**
 * Function will save the booked seat details to DB
 *
 * @param array $seats
 * @param Object $db
 * @return array
 */
function bookMySeat(array $seats, Object $db): array
{
	try {
		$bookedSeats = [];
		// For now just taken time for ticket number, later it an be made more unique
		$ticketNumber = time();
		$status = 1; // this is the active booked status
		$createdAt = date('Y-m-d H:i:s'); // Datetime when booking done
		$db->begin_transaction(); // Transaction as we are doing inserstion in multiple tables

		// SQL query for the inserting data in ticket table
		$sqlQuery = $db->prepare('INSERT INTO `tickets`( ticket_number, status, created_at) VALUES (?,?,?)');
		// Bind parameters for the prepare statement
		$sqlQuery->bind_param("sss", $ticketNumber, $status, $createdAt);
		$sqlQuery->execute(); // execute the query

		// This will get the last id stored i.e ticket_id which we need for next table
		$ticketId = $db->insert_id; 

		// we will be looping each seat number from the array to store in ticket_details table
		foreach ($seats as $seat) {
			// SQL query for the inserting data in ticket table
			$bookSeatQuery = $db->prepare('INSERT INTO `ticket_details`( ticket_id, seat_number) VALUES (?,?)');
			// Bind parameters for the prepare statement
			$bookSeatQuery->bind_param("ss", $ticketId, $seat);
			$bookSeatQuery->execute(); // execute the query

			// will get the last id from each query, which is use to prepare the json response to client
			$bookedSeats["booked"][$seat] = $db->insert_id;
		}
	    // If we arrive here, it means that no exception was thrown and we can commit the transaction
	    $db->commit();
	    return $bookedSeats;
	} catch (\Exception $e) { // we can catch the exception in case any error occurs
	    // An exception has been thrown and We must rollback the transaction
	    $db->rollback();
	    // We can use the custom error messageto send to the users instead : $e->getMessage();
	    return ["message" => "Failed to create tickets due to exception"]; 
	}
}

/**
 * Function will filter and get the array with adjecent seats available
 * If adjecent seats not available response will be empty array
 *
 * @param array $availableSeats
 * @param int $requiredSeats
 * @return array
 */
function getAdjecentSeats(array $availableSeats, int $requiredSeats): array
{
	$remainingSeats = $requiredSeats;
	$seatsArray = [];
	foreach ($availableSeats as $key => $numberOfSeats) {
		if(empty($availableSeats[$key+1]) && $remainingSeats>$numberOfSeats) {
			$seatsArray = [];
			$remainingSeats = $requiredSeats;
		} else {
			$seatsArray[$key] = $numberOfSeats;
			$remainingSeats -= $numberOfSeats;
		}
	}
	return $seatsArray;	
}