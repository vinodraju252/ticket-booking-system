<?php
$serverName = "localhost";
$userName = "root";
$password = "root";
$databaseName = "bookings";

// Create mysqli connection
$db = new mysqli($serverName, $userName, $password, $databaseName);

// Check connection
if ($db->connect_error) {
	// If connection fails then die here
  	die("Connection failed: " . $db->connect_error);
}