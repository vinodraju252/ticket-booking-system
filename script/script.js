let baseUrl = 'http://localhost:8080/'; // set base URL

// Creating Our XMLHttpRequest object 
let xhr = new XMLHttpRequest();
let bookingForm = document.getElementById("saveBookings");
// When submit button is clicked then when we can call this eventListener to save bookings
bookingForm.addEventListener("submit", (e) => {
    e.preventDefault();
    var formData = new FormData();
    formData.append("numberOfSeats", document.getElementById("numberOfSeats").value);
    let url = baseUrl + 'bookings/api/save_bookings.php';
    xhr.open("POST", url, true); // will be a post request
    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let response = JSON.parse(this.responseText)
            showBookingMessage(response);
            getData(); // will refresh the booking data in the UI
            document.getElementById("numberOfSeats").value = ""; // Reset the input field
        }
    }
    xhr.send(formData);
});

// To get the data for stored bookings
function getData() 
{
    // Making our connection  
    let url = baseUrl + 'bookings/api/get_bookings.php';
    xhr.open("GET", url, true);

    // function execute after request is successful 
    xhr.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            let response = JSON.parse(this.responseText)
            populateSeats(response["seat_availablity"]);
            populateRecentBookings(response["recent_bookings"]);
        }
    }

    // Sending our request 
    xhr.send();
}

// Populate data to client UI
function populateSeats(bookingsObject) 
{
    let html = '';
    let bookingStatus = null;
    let title = null;
    Object.entries(bookingsObject).forEach(([key, seatsObject]) => {
        html += `<div class="row mb-2"><div class='col'>`;
        Object.entries(seatsObject).forEach(([seatNumber, status]) => {
            bookingStatus = (status == 'A') ? 'success' : 'danger';
            title = (status == 'A') ? 'Available' : 'Booked';
            html += `<button title='`+title+`' class='btn btn-outline-`+bookingStatus+` btn-sm button-style'>`+seatNumber+`</button>`;
        });
        html += `</div></div>`;
    }); 
    document.getElementById("booking-details").innerHTML = html;
}

// Populate recent bookings to the UI
function populateRecentBookings(bookingsObject) 
{
    let html = '';
    // will be looping the response object to show itration data to UI
    Object.entries(bookingsObject).forEach(([key, seatsObject]) => {
        html += `<div class="row mb-2"><div class='col'>`;
            html += `<div class="alert alert-success" role="alert">`;
            html += `<b>Ticket Number : </b>    `+ seatsObject["ticket_number"] + `<br/>`;
            html += `<b>Seat Numbers : </b>     `+ seatsObject["seats"]+`<br/>`; 
            html += `<b>Number of seats : </b>  `+seatsObject["seat_count"]+`</div>`;
        html += `</div></div>`;
    }); 
    document.getElementById("recent-bookings").innerHTML = html;
}

// Show booking message once the booking done
function showBookingMessage(response) 
{
    let html = `<div class="row mt-2"><div class='col'>`;
        html += `<div class="alert alert-success" role="alert">`;
        html += response["message"]
        html += `</div></div>`;
    document.getElementById("booking-message").innerHTML = html;
}
getData();